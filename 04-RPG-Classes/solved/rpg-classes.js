// Paste your code from the previous activity `03-RPG-Prototypes`

// Create a constructor function called `Character` that takes in 3 arguments: `name`, `strength`, `hitpoints`
// YOUR CODE HERE
//
// function Character(name, strength, hitpoints) {
//     this.name = name,
//     this.strength = strength,
//     this.hitpoints = hitpoints
// }
// Create a prototype method called `printStats()` which prints all of the stats for a character
// YOUR CODE HERE
//
// Character.prototype.printStats = function () {
//     console.log(this.name, this.strength, this.hitpoints)
// }
// Create a prototype method called `isAlive()` which prints whether or not this character is alive
// by determining whether their `hitpoints` are above or below zero
// YOUR CODE HERE
//
// Character.prototype.isAlive = function() {
//     if (this.hitpoints > 0) return console.log(`${this.name} is alive!`);
//     return console.log(`${this.name} is dead!`);
// }
// Create a prototype method called `attack()` which takes in a second character
// and subtracts this character's `strength` from their `hitpoints`
// YOUR CODE HERE
//
// Character.prototype.attack = function (fighter) {
//     fighter.hitpoints -= this.strength;
// }
// =============================================================

// let warrior = new Character('Crusher', 10, 75);
// let rogue = new Character('Dodger', 20, 50);

// warrior.printStats();
// rogue.printStats();

// rogue.attack(warrior);
// warrior.printStats();
// warrior.isAlive();

// Convert the constructor function, including the prototype methods, to ES6 Classes.

class Character {
    constructor(name, strength, hitpoints) {
        this.name = name,
        this.strength = strength,
        this.hitpoints = hitpoints
    }

    printStats() {
        console.log(this.name, this.strength, this.hitpoints);
    };

    isAlive() {
        if (this.hitpoints > 0) return console.log(`${this.name} is alive!`);
        return console.log(`${this.name} is dead!`);
    };

    attack(fighter) {
        console.log(`${this.name} attacks ${fighter.name}!`)
        fighter.hitpoints -= this.strength;
    };
}

// =============================================================

// Create two new instances of a `Character`, giving them different names, strength, and hitPoints.

let warrior = new Character('harry', 30, 100);
let rogue = new Character('spiffy', 50, 60);

// Call a combination of `printStats()`, `attack()`, and `isAlive()` methods to have the two characters "fight" in your console.

warrior.printStats();
rogue.printStats();

rogue.attack(warrior);
warrior.isAlive();
warrior.printStats();

warrior.attack(rogue);
rogue.isAlive();
rogue.printStats();