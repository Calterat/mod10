// =============================================================
// PART 1

function MiniBank(balance) {
  this.balance = balance;
  this.getBalance = function() {
    return this.balance;
  },
    // Add a `setBalance()` function
    // YOUR CODE HERE
    //
    this.setBalance = function(value) {
      this.balance = value;
    },

    // Add a `deposit()` function
    // YOUR CODE HERE
    //
    this.deposit = function(value) {
      if (value > 0) {
        let newBalance = this.getBalance()+value;
        this.setBalance(newBalance);
        console.log(`Deposited $${value}!`);
      } else if (value === 0 || value < 0) {
        console.log('You cannot deposit that.');
      }
    },

    // Add a `withdraw()` function
    // YOUR CODE HERE
    //
    this.withdraw = function(value) {
      let newBalance = this.getBalance()-value;
      if (newBalance>0) {
        this.setBalance(newBalance);
        console.log(`Withdrew $${value}!`)
      }
    },

    this.printBalance = function() {
      console.log(`Balance: ${this.getBalance()}`);
    };
}

// =============================================================
// PART 2

// Create a new `bank` object
const bank = new MiniBank(100);
// Print the `bank` balance
bank.printBalance();
// Deposit some money and then print the `bank` balance
bank.deposit(-1);
bank.printBalance();
// Withdraw some money and then print the `bank` balance
bank.withdraw(30);
bank.printBalance();
